// There are 2N people a company is planning to interview. The cost of flying the i-th person to city A is costs[i][0], and the cost of flying the i-th person to city B is costs[i][1].
// Return the minimum cost to fly every person to a city such that exactly N people arrive in each city.


class Solution {
    public int twoCitySchedCost(int[][] costs) {

    Arrays.sort(costs,Comparator.comparingInt(o->o[0]-o[1]));
        int sum=0;
        for(int i=0; i < costs.length; i++){
            if(i<costs.length/2) {
             sum+=costs[i][0];   
            }else{
                sum+=costs[i][1];
            }
        }
         return sum;
         
    }
         
}