/*Given an integer array arr, count element x such that x + 1 is also in arr.

If there're duplicates in arr, count them seperately. */

class Solution {
    public int countElements(int[] arr) {
        Set<Integer> st = new HashSet<>();
        for(int val: arr){
            st.add(val);
        }
        int result = 0;
        for(int i=0; i < arr.length; i++){
            if(st.contains(arr[i]+1)){
                result++;
            }
        }
        return result;
    }
}