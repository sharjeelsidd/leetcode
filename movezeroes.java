/* Given an array nums, 
write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements. */


class Solution {
    public void moveZeroes(int[] nums) {
        int idx = 0;
        for(int i=0; i< nums.length; i++){
            if(nums[i] != 0){
                nums[idx++] = nums[i];
            }
        }
        while(idx < nums.length){
            nums[idx++] = 0;
        }
           return;
    }
 
}